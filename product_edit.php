
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Kaiser-Logistics</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
	  
	  form {
		margin: 4em;
	  }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="#">Kaiser-Logistics</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Home</a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="scanner.html">Scanner</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#">Add Product<span class="sr-only">(current)</span></a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
	<main role="main" class="container">
	
	<?php
		$code = $_GET['inputId'];
		$name = "";
		$location = "";
		$description = "";
			
		if($code != ""){
		
			$pdo = new PDO('mysql:host=localhost;dbname=logistic', 'root', 'root');
			
			$stmt = $pdo->prepare("SELECT * FROM products WHERE ID = :code");
			$stmt->execute(array('code' => $code));
			
			foreach ($stmt as $row) {
				$name = $row['name'];
				$location = $row['location'];
				$description = $row['description'];
			}
		}
	?>
		<form method="POST" action="index.php">
		  <div class="form-group">
			<label for="inputId">Product ID:</label>
			<input type="text" class="form-control" id="inputId" name="inputId" placeholder="Enter valid Code" value="<?php	echo $code; ?>">
		  </div>
		  
		  <div class="form-group">
			<label for="inputName">Product Name:</label>
			<input type="text" class="form-control" id="inputName" name="inputName" placeholder="Enter name" value="<?php echo $name; ?>">
		  </div>
		  
		  <div class="form-group">
			<label for="inputLocation">Location:</label>
			<input type="text" class="form-control" id="inputLocation" name="inputLocation" placeholder="Enter Location" value="<?php echo $location; ?>">
		  </div>
		  
		  <div class="form-group">
			<label for="inputDescription">Description:</label>
			<textarea class="form-control" id="inputDescription" name="inputDescription" placeholder="Description"><?php echo $description; ?></textarea>
		  </div>
		  
		  <div class="form-group">
			<input type="checkbox" class="form-check-input" name="exampleCheck1" id="exampleCheck1">
			<label class="form-check-label" for="exampleCheck1">Delete?</label>
		  </div>
		  <button type="submit" class="btn btn-primary">Save</button>
		</form>
	</main>
	  <!-- Bootstrap core JavaScript -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>

</body>
</html>
