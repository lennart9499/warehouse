
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Kaiser-Logistics</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="#">Kaiser-Logistics</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="scanner.html">Scanner</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="product_edit.php">Add Product</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>

	<?php
		if(isset($_POST['inputId'])){
			$code = $_POST['inputId'];
			$name = $_POST['inputName'];
			$location = $_POST['inputLocation'];
			$description = $_POST['inputDescription'];
			
			$date = date("Y-m-d H:i:s");
			
			if(!isset($pdo))
				$pdo = new PDO('mysql:host=localhost;dbname=logistic', 'root', 'root');
			
			if(isset($_POST['exampleCheck1']) && $_POST['exampleCheck1'] == "on"){
				$stmt = $pdo->prepare("DELETE FROM `products` WHERE `id` = :code;");
				$stmt->execute(array('code' => $code));
			} else {
				$stmt = $pdo->prepare("REPLACE INTO products(id, name, location, description, created_at) VALUES (:code, :name, :location, :description, :date)");
				$stmt->execute(array('code' => $code, 'name' => $name, 'location' => $location, 'description' => $description, 'date' => $date));
			}
			
			$_POST['inputId'] = "";
		}
	?>
	<main role="main" class="container">
		<table style="margin-top: 5em;"id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
		  <thead>
			<tr>
			  <th class="th-sm">ID
			  </th>
			  <th class="th-sm">Name
			  </th>
			  <th class="th-sm">Location
			  </th>
			  <th class="th-sm">Description
			  </th>
			  <th class="th-sm">Created_at
			  </th>
			</tr>
		  </thead>
		  <tbody>
		  
			<?php 
			if(!isset($pdo))
				$pdo = new PDO('mysql:host=localhost;dbname=logistic', 'root', 'root');
			
			$sql = "SELECT * FROM products";
			$name = "";
			$location = "";
			$description = "";
			foreach ($pdo->query($sql) as $row) {
				$code = $row['id'];
				$name = $row['name'];
				$location = $row['location'];
				$description = $row['description'];
				$date = $row['created_at'];
				?>
				<tr>
				  <td><?php echo $code; ?></td>
				  <td><?php echo $name; ?></td>
				  <td><?php echo $location; ?></td>
				  <td><?php echo $description; ?></td>
				  <td><?php echo $date; ?></td>
				  <td><a href="product_edit.php?inputId=<?php echo $code; ?>">Edit</a></td>
				</tr>
			<?php } ?>
			
			</tbody>
		</table>
	</main>
	  <!-- Bootstrap core JavaScript -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>

</body>
</html>











