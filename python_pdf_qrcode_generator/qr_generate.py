# add_image.py

import qrcode
import random
import string
from fpdf import FPDF


def add_image(image_path):
    pdf = FPDF()
    pdf.add_page()
    pdf.image(image_path, x=0, y=0, w=20)
    # pdf.set_font("Arial", size=12)
    # pdf.ln(85)  # move 85 down
    # pdf.cell(200, 10, txt="{}".format(image_path), ln=1)
    pdf.output("add_image.pdf")


if __name__ == '__main__':

    import os
    from fnmatch import fnmatch

    for dirpath, dirnames, filenames in os.walk(os.curdir):
        for file in filenames:
            if fnmatch(file, '*.png'):
                os.remove(os.path.join(dirpath, file))

    # Create qr code instance
    pdf = FPDF()
    pdf.add_page()

    i = -1
    while i < 8:
        i += 1
        j = -1
        while j < 6:
            j += 1
            qr = qrcode.QRCode(
                version=1,
                error_correction=qrcode.constants.ERROR_CORRECT_H,
                box_size=1,
                border=4,
            )

            data = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))

            # Add data
            qr.add_data(data)
            qr.make(fit=True)
            img = qr.make_image(fill_color="black", back_color="white")

            off = 0
            if j % 2 == 0:
                img = img.rotate(90)
                off -= 5
            else:
                img = img.rotate(-90)
                off += 5

            img.save("temp/image" + data + ".png")

            if j == 0 or j == 5:
                off = 0
            pdf.image("temp/image" + data + ".png", x=(0.99 * ((j * 37) + off)), y=2 + (i * 38), w=26)

            ## TEXT
            from PIL import Image, ImageDraw, ImageFont

            W, H = (120, 30)
            img = Image.new('RGB', (W, H), color=(255, 255, 255))

            fnt = ImageFont.truetype('Roboto-Bold.ttf', 26)
            d = ImageDraw.Draw(img)
            w, h = d.textsize(data, fnt)
            d.text(((W-w)/2,(H-h)/2), data, font=fnt, fill=(0, 0, 0))


            off = 0
            if j % 2 == 0:
                img = img.rotate(90, expand=1)
                off += 23
            else:
                img = img.rotate(-90, expand=1)
                off += 1
            img.save('temp/text' + data + '.png')

            if j >= 2:
                off -= 5
            if j >= 3:
                off += 4
            if j >= 4:
                off -= 4
            if j >= 5:
                off += -2

            pdf.image("temp/text" + data + ".png", x=(j * 37) + off, y=2 + (i * 38), w=7)

    pdf.output("add_image.pdf")
